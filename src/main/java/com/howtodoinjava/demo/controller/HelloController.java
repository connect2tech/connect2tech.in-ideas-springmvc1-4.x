package com.howtodoinjava.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String entry(Model model) {
		return "hello";
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(Model model) {
		return "welcomeUser";
	}

	@RequestMapping(value = "/setData", method = RequestMethod.GET)
	public String setData(Model model) {
		return "dataForm";
	}

	@RequestMapping(value = "/processData", method = RequestMethod.GET)
	public String processData(HttpServletRequest request, Model model) {

		String firstname = request.getParameter("firstname");
		model.addAttribute("first_name",firstname.toUpperCase());

		return "processedData";
	}

}